﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenusManager : MonoBehaviour
{
    public GameObject pause, Win, Lose,game;
     Player pl;
    // Start is called before the first frame update
    void Start()
    {
        game.SetActive(true);
        pause.SetActive(false);
        Win.SetActive(false);
        Lose.SetActive(false);
        pl = GameObject.Find("player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0;
            game.SetActive(false);
            pause.SetActive(true);
            Win.SetActive(false);
            Lose.SetActive(false);
        }
        if (pl.win==true)
        {
            game.SetActive(false);
            pause.SetActive(false);
            Win.SetActive(true);
            Lose.SetActive(false);
            Time.timeScale = 0;
        }
        if (pl.dead==true)
        {
            game.SetActive(false);
            pause.SetActive(false);
            Win.SetActive(false);
            Lose.SetActive(true);
            Time.timeScale = 0;
        }
    }
    public void Menu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void retry()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }
    public void continueGame()
     {
        Time.timeScale = 1;
        game.SetActive(true);
        pause.SetActive(false);
        Win.SetActive(false);
        Lose.SetActive(false);
    }
}
