﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class could : MonoBehaviour
{
    public float time,startTime=0;
    public GameObject c;
    public bool solid = true;
    BoxCollider2D bc;
    float t,tc=0;
    // Start is called before the first frame update
    void Start()
    {
        t = 0;
        solid = true;
        bc = gameObject.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
       if(tc<=startTime) {
            tc += Time.deltaTime;
        }
        else if (tc>=startTime)
        {
            t += Time.deltaTime;
        }
        if (t>=time &&solid==true&&tc>=startTime)
        {
            bc.enabled = false;
            c.SetActive(false);
            solid = false;
            t = 0;
        }else if (t >= time && solid == false && tc >= startTime)
        {
            bc.enabled = true;
            c.SetActive(true);
            solid = true;
            t = 0;
        }
    }
}
