﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class canon : MonoBehaviour
{
    public float TimeShot,startTime=0;
    bool canshot = true;
    public GameObject ball,shotpoint;
    float t,tc;
    // Start is called before the first frame update
    void Start()
    {
        tc = 0;
        t = 0;
        canshot = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (tc <= startTime)
        {
            tc += Time.deltaTime;
        }


        Vector2 so = shotpoint.transform.position;
        if (canshot==true&&tc>=startTime)
        {
            Instantiate(ball,so,Quaternion.identity);
            canshot = false;
            t = Time.time;
        }else if (Time.time>=t+TimeShot&&canshot==false&&tc>=startTime)
        {
            canshot = true;
        }
    }
}
