﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class enemi : MonoBehaviour
{
    public float vel=10;
    public GameObject LimitL, LimitR;
    public bool noDeath = false;
    public int di=1,po=1;
    Rigidbody2D rb;
    Player pl;
    // Start is called before the first frame update
    void Start()
    {
        pl = GameObject.Find("player").GetComponent<Player>();
        rb = gameObject.GetComponent<Rigidbody2D>();
     
    }

    // Update is called once per frame
    private void Update()
    {
        if (rb.velocity.x >0)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }
    }
    private void FixedUpdate()
    { 
        Vector3 po = gameObject.transform.position;
        rb.velocity = new Vector2(di*vel,0);
        if (po.x>LimitR.transform.position.x)
        {
            di = -1;
        }else if (po.x < LimitL.transform.position.x)
        {
            di = 1;
        }
       
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("damage")&&noDeath==false)
        {
            pl.points += po;
            Destroy(gameObject);
        }
    }
}
