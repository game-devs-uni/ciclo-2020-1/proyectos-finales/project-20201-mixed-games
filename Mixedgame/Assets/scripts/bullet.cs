﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet: MonoBehaviour
{
    public float vel=100f;
    Rigidbody2D rb;
    public float dest = 2;
    float   t;
    // Start is called before the first frame update
    void Start()
    {
        t = 0;
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        t += Time.deltaTime;

    }
    private void FixedUpdate()
    {
        rb.velocity = gameObject.transform.right*vel*Time.deltaTime;
        if (t>=dest)
        {
            Destroy(gameObject);
        }
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.x!=0&&collision.transform.CompareTag("damage")==false) {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("damage"))
        {
            Destroy(gameObject);
        }
    }
}
