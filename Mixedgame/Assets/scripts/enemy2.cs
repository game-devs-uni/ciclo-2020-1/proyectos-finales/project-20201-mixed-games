﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy2 : MonoBehaviour
{
    public float vel=3;
    public GameObject LU, LD;
    Rigidbody2D rb;
    public bool noDeath = false;
    int d = 1;
    public int po=2;
    Player pl;
    // Start is called before the first frame update
    void Start()
    {
        pl = GameObject.Find("player").GetComponent<Player>();
        rb =gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    { Vector2 po = gameObject.transform.position;
        rb.velocity = new Vector2(0,d*vel);
        if (po.y >= LU.transform.position.y)
            d = -1;
        else if (po.y <= LD.transform.position.y)
            d = 1;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("damage") && noDeath == false)
        {
            Destroy(gameObject);
            pl.points += po;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("damage") && noDeath == false)
        {
            pl.points += po;
            Destroy(gameObject);
        }
    }
}
